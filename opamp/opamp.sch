EESchema Schematic File Version 4
LIBS:main-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R13
U 1 1 60B9075A
P 4000 3250
F 0 "R13" H 4070 3296 50  0000 L CNN
F 1 "1k" H 4070 3205 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 3930 3250 50  0001 C CNN
F 3 "~" H 4000 3250 50  0001 C CNN
	1    4000 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 60B913FB
P 4500 3250
F 0 "R14" H 4570 3296 50  0000 L CNN
F 1 "1k" H 4570 3205 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4430 3250 50  0001 C CNN
F 3 "~" H 4500 3250 50  0001 C CNN
	1    4500 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R15
U 1 1 60B91BE1
P 4500 3700
F 0 "R15" H 4570 3746 50  0000 L CNN
F 1 "1k" H 4570 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4430 3700 50  0001 C CNN
F 3 "~" H 4500 3700 50  0001 C CNN
	1    4500 3700
	1    0    0    -1  
$EndComp
$Comp
L Diode:BZX84Cxx D4
U 1 1 60B9508E
P 4000 3750
F 0 "D4" V 3954 3830 50  0000 L CNN
F 1 "BZX84Cxx" V 4045 3830 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" H 4000 3575 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzx84c2v4.pdf" H 4000 3750 50  0001 C CNN
	1    4000 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 3400 4000 3500
Wire Wire Line
	4500 3400 4500 3500
Wire Wire Line
	4500 3500 4700 3500
Wire Wire Line
	4700 3500 4700 3300
Wire Wire Line
	4700 3300 5200 3300
Connection ~ 4500 3500
Wire Wire Line
	4500 3500 4500 3550
Wire Wire Line
	5000 3500 4800 3500
Wire Wire Line
	4500 3100 4500 3000
Wire Wire Line
	4500 3000 4250 3000
Wire Wire Line
	4250 3000 4250 3500
Wire Wire Line
	4250 3500 4000 3500
Connection ~ 4000 3500
Wire Wire Line
	4000 3500 4000 3600
$Comp
L power:GND #PWR0122
U 1 1 60B97809
P 4600 4000
F 0 "#PWR0122" H 4600 3750 50  0001 C CNN
F 1 "GND" H 4605 3827 50  0000 C CNN
F 2 "" H 4600 4000 50  0001 C CNN
F 3 "" H 4600 4000 50  0001 C CNN
	1    4600 4000
	1    0    0    -1  
$EndComp
Connection ~ 4600 4000
Wire Wire Line
	4900 3700 4900 3850
Wire Wire Line
	4000 3100 4000 3000
Wire Wire Line
	4600 4000 4800 4000
Wire Wire Line
	4000 4000 4500 4000
Wire Wire Line
	5200 3900 5200 4000
Wire Wire Line
	4500 3850 4500 4000
Connection ~ 4500 4000
Wire Wire Line
	4500 4000 4600 4000
Wire Wire Line
	4000 4000 4000 3900
Wire Wire Line
	4800 3500 4800 4000
Connection ~ 4800 4000
Wire Wire Line
	4800 4000 5200 4000
$Comp
L AD8001ANZ:AD8001ANZ IC2
U 1 1 60BB23DF
P 5100 3500
F 0 "IC2" H 5650 3765 50  0000 C CNN
F 1 "AD8001ANZ" H 5650 3674 50  0000 C CNN
F 2 "AD8001ANZ:DIP794W56P254L928H457Q8N" H 6050 3600 50  0001 L CNN
F 3 "http://docs-emea.rs-online.com/webdocs/077f/0900766b8077ff27.pdf" H 6050 3500 50  0001 L CNN
F 4 "AD8001ANZ, Video Amplifier 1200V/us, 8-Pin PDIP" H 6050 3400 50  0001 L CNN "Description"
F 5 "4.57" H 6050 3300 50  0001 L CNN "Height"
F 6 "Analog Devices" H 6050 3200 50  0001 L CNN "Manufacturer_Name"
F 7 "AD8001ANZ" H 6050 3100 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "584-AD8001ANZ" H 6050 3000 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD8001ANZ?qs=%2FtpEQrCGXCz%2FglmQYPZR1w%3D%3D" H 6050 2900 50  0001 L CNN "Mouser Price/Stock"
F 10 "AD8001ANZ" H 6050 2800 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/ad8001anz/analog-devices" H 6050 2700 50  0001 L CNN "Arrow Price/Stock"
	1    5100 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3500 5000 3600
Wire Wire Line
	5000 3600 5100 3600
Wire Wire Line
	4900 3700 5100 3700
Wire Wire Line
	5200 3300 5200 3350
Wire Wire Line
	5200 3350 4850 3350
Wire Wire Line
	4850 3350 4850 3900
Wire Wire Line
	4850 3900 5100 3900
Wire Wire Line
	5100 3900 5100 3800
Wire Wire Line
	5200 3900 5250 3900
Wire Wire Line
	5250 3900 5250 4050
Wire Wire Line
	5250 4050 6300 4050
Wire Wire Line
	6300 4050 6300 3600
Wire Wire Line
	6300 3600 6200 3600
NoConn ~ 5100 3500
NoConn ~ 6200 3500
NoConn ~ 6200 3800
Text GLabel 4900 3850 3    50   Input ~ 0
vin(5v)
Text GLabel 4000 3000 0    50   Input ~ 0
12v
Text GLabel 6200 3700 2    50   Input ~ 0
3.3v
$EndSCHEMATC
